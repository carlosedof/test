import styled from 'styled-components';

export const Container = styled.div`
    padding: 2% 8%;
`;

export const PageTitle = styled.h3`
    color: white;
    font-family: montserrat-bold;
    text-align: center;
    font-size: x-large;
    margin: 5% 0;
`;

export const PageSubtitle = styled.h5`
    color: white;
    font-family: montserrat-medium;
    text-align: center;
    font-size: medium;
`;
