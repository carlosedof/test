import ApolloClient, {gql} from 'apollo-boost';

export default new ApolloClient({
    uri: 'https://marvelql.herokuapp.com/',
    resolvers: {
        Mutation: {
            updateCharacter: (_, variables, { cache }) => {
                const query = gql`
                    query GetCharacters {
                      characters @client {
                        id
                        name
                        description
                      }
                    }
                  `;
                const id = `Character:${variables.id}`;
                const data = { ...variables };
                cache.writeData({ id, data });
                return null;
            }
        },
    },
});
